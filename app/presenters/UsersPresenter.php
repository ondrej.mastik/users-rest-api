<?php

namespace App\Presenters;

use Nette\Application\Responses\JsonResponse;

class UsersPresenter extends \Drahak\Restful\Application\UI\ResourcePresenter {

    /** @var \App\Model\Users @inject */
    public $users;

    public function actionRead($id) {
        $data = $this->users->read($id);

        if($data) {
            $this->sendResponse(new JsonResponse($data));
        }

        $this->error("Záznam s id $id nenalezen.", \Nette\Http\Response::S404_NOT_FOUND);
    }

    public function actionCreate() {
        if(!$this->users->create($this->getInput()->getData())) {
            $this->error("Některá data chybí", \Nette\Http\Response::S400_BAD_REQUEST);
        }
    }

    public function actionUpdate($id) {
        if(!$this->users->update($id, $this->getInput()->getData())) {
            $this->error("Záznam s id $id nenalezen.", \Nette\Http\Response::S404_NOT_FOUND);
        }
    }

    public function actionDelete($id) {
        if(!$this->users->delete($id)) {
            $this->error("Záznam s id $id nenalezen.", \Nette\Http\Response::S404_NOT_FOUND);
        }
    }

}